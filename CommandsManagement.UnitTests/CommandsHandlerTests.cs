﻿using CommandManagement;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace CommandsManagement.UnitTests
{
    [TestClass]
    public class CommandsHandlerTests
    {
        private CommandsHandler commandsHandler;

        [TestInitialize]
        public void SetUp()
        {
            commandsHandler = new CommandsHandler();
        }

        [TestCleanup]
        public void TearDown()
        {
            commandsHandler.ClearCache();
        }

        #region RegisterAttributedCommandHandlers

        [TestMethod]
        [ExpectedExceptionWithMessage(typeof(ArgumentException), "owner cannot be null.")]
        public void TestRegisterAttributedCommandHandlersWithNullArgument()
        {
            commandsHandler.RegisterAttributedCommandHandlers(null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "IncorrectMethod must has one CommandArgs type parameter or none.")]
        public void TestMethodSignatureValidation()
        {
            commandsHandler.RegisterAttributedCommandHandlers(new AttributedCommandsHandlerWithIncorrectParameters());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "IncorrectMethod method cannot return any value.")]
        public void TestReturnTypeValidationOfExecuteCommandHandler()
        {
            commandsHandler.RegisterAttributedCommandHandlers(new AttributedCommandsHandlerWithIncorrectExecuteCommandReturnType());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "IncorrectMethod method must return a result of type bool.")]
        public void TestReturnTypeValidationOfCanExecuteCommandHandler()
        {
            commandsHandler.RegisterAttributedCommandHandlers(new AttributedCommandsHandlerWithIncorrectCanExecuteCommandReturnType());
        }

        #endregion

        #region ClearCache

        [TestMethod]
        public void TestClearCache()
        {
            commandsHandler.RegisterAttributedCommandHandlers(new AttributedCommandsHandler());
            commandsHandler.ClearCache();
        }

        #endregion

        #region ContainsCommandHandler

        [TestMethod]
        public void TestContainsCommandHandler()
        {
            commandsHandler.RegisterAttributedCommandHandlers(new AttributedCommandsHandler());

            bool result1 = commandsHandler.ContainsCommandHandler(CommandHandlerType.Execute, "METHOD1");
            bool result2 = commandsHandler.ContainsCommandHandler(CommandHandlerType.CanExecute, "METHOD1");
            bool result3 = commandsHandler.ContainsCommandHandler(CommandHandlerType.Execute, "METHOD2");
            bool result4 = commandsHandler.ContainsCommandHandler(CommandHandlerType.CanExecute, "METHOD2");
            bool result5 = commandsHandler.ContainsCommandHandler(CommandHandlerType.Execute, "METHOD3");
            bool result6 = commandsHandler.ContainsCommandHandler(CommandHandlerType.CanExecute, "METHOD3");

            Assert.IsTrue(result1);
            Assert.IsTrue(result2);
            Assert.IsTrue(result3);
            Assert.IsFalse(result4);
            Assert.IsFalse(result5);
            Assert.IsTrue(result6);
        }

        #endregion

        #region UnregisterCommandHandler

        [TestMethod]
        public void TestUnregisterCommandHandler()
        {
            commandsHandler.RegisterAttributedCommandHandlers(new AttributedCommandsHandler());

            commandsHandler.UnregisterCommandHandler("METHOD1");

            bool result1 = commandsHandler.ContainsCommandHandler(CommandHandlerType.Execute, "METHOD1");
            bool result2 = commandsHandler.ContainsCommandHandler(CommandHandlerType.CanExecute, "METHOD1");
            bool result3 = commandsHandler.ContainsCommandHandler(CommandHandlerType.Execute, "METHOD2");
            bool result4 = commandsHandler.ContainsCommandHandler(CommandHandlerType.CanExecute, "METHOD2");

            Assert.IsFalse(result1);
            Assert.IsFalse(result2);
            Assert.IsTrue(result3);
            Assert.IsFalse(result4);
        }

        #endregion

        #region ExecuteCommandHandler

        [TestMethod]
        public void TesExecuteCommandHandler()
        {
            commandsHandler.RegisterAttributedCommandHandlers(new AttributedCommandsHandler());

            try
            {
                commandsHandler.ExecuteCommandHandler(new CommandArgs { CommandName = "METHOD1" });

                Assert.Fail("An exception should have been thrown from ExecuteMethod1.");
            }
            catch (Exception ex)
            {
                Assert.AreEqual("Exception from command handler.", ex.Message);
            }

            try
            {
                commandsHandler.ExecuteCommandHandler(new CommandArgs { CommandName = "METHOD2" });
            }
            catch
            {
                Assert.Fail("ExecuteMethod2 should not throw exception.");
            }
        }

        [TestMethod]
        [ExpectedExceptionWithMessage(typeof(ArgumentException), "Command handler METHOD3 does not exist.")]
        public void TestUnregisteredExecuteCommandHandler()
        {
            commandsHandler.RegisterAttributedCommandHandlers(new AttributedCommandsHandler());

            commandsHandler.ExecuteCommandHandler(new CommandArgs { CommandName = "METHOD3" });
        }

        #endregion

        #region CanExecuteCommandHandler

        [TestMethod]
        public void TestCanExecuteCommandHandler()
        {
            commandsHandler.RegisterAttributedCommandHandlers(new AttributedCommandsHandler());

            bool result1 = commandsHandler.CanExecuteCommandHandler(new CommandArgs { CommandName = "METHOD1" });
            bool result2 = commandsHandler.CanExecuteCommandHandler(new CommandArgs { CommandName = "METHOD3" });

            Assert.IsFalse(result1);
            Assert.IsTrue(result2);
        }

        [TestMethod]
        [ExpectedExceptionWithMessage(typeof(ArgumentException), "Command handler METHOD2 does not exist.")]
        public void TestUnregisteredCanExecuteCommandHandler()
        {
            commandsHandler.RegisterAttributedCommandHandlers(new AttributedCommandsHandler());

            bool result = commandsHandler.CanExecuteCommandHandler(new CommandArgs { CommandName = "METHOD2" });
        }

        #endregion

        #region GetExecuteCommandHandler

        [TestMethod]
        public void TestGetExecuteCommandHandler()
        {
            commandsHandler.RegisterAttributedCommandHandlers(new AttributedCommandsHandler());

            Action<CommandArgs> executeHandler = commandsHandler.GetExecuteCommandHandler("METHOD1");

            try
            {
                executeHandler(new CommandArgs());

                Assert.Fail("An exception should have been thrown from ExecuteMethod1.");
            }
            catch (Exception ex)
            {
                Assert.AreEqual("Exception from command handler.", ex.Message);
            }
        }

        [TestMethod]
        [ExpectedExceptionWithMessage(typeof(ArgumentException), "Command handler METHOD100 does not exist.")]
        public void TestUnregisterdGetExecuteCommandHandler()
        {
            commandsHandler.RegisterAttributedCommandHandlers(new AttributedCommandsHandler());

            Action<CommandArgs> executeHandler = commandsHandler.GetExecuteCommandHandler("METHOD100");
        }

        #endregion

        #region GetCanExecuteCommandHandler

        [TestMethod]
        public void TestGetCanExecuteCommandHandler()
        {
            commandsHandler.RegisterAttributedCommandHandlers(new AttributedCommandsHandler());

            Func<CommandArgs, bool> executeHandler = commandsHandler.GetCanExecuteCommandHandler("METHOD1");
            bool result = executeHandler(new CommandArgs());

            Assert.IsFalse(result);
        }

        [TestMethod]
        [ExpectedExceptionWithMessage(typeof(ArgumentException), "Command handler METHOD100 does not exist.")]
        public void TestUnregisterdGetCanExecuteCommandHandler()
        {
            commandsHandler.RegisterAttributedCommandHandlers(new AttributedCommandsHandler());

            Func<CommandArgs, bool> executeHandler = commandsHandler.GetCanExecuteCommandHandler("METHOD100");
        }

        #endregion
    }

    class AttributedCommandsHandler
    {
        [ExecuteCommand("METHOD1")]
        public void ExecuteMethod1(CommandArgs args) 
        { 
            throw new Exception("Exception from command handler.");
        }

        [CanExecuteCommand("METHOD1")]
        public bool CanExecuteMethod1(CommandArgs args) 
        { 
            return false; 
        }

        [ExecuteCommand("METHOD2")]
        public void ExecuteMethod2() { }

        [CanExecuteCommand("METHOD3")]
        public bool CanExecuteMethod3() 
        { 
            return true; 
        }
    }

    class AttributedCommandsHandlerWithIncorrectParameters
    {
        [ExecuteCommand("INCORRECT.METHOD")]
        public void IncorrectMethod(CommandArgs args, int number) { }
    }

    class AttributedCommandsHandlerWithIncorrectExecuteCommandReturnType
    {
        [ExecuteCommand("INCORRECT.METHOD")]
        public string IncorrectMethod() 
        { 
            return string.Empty; 
        }
    }

    class AttributedCommandsHandlerWithIncorrectCanExecuteCommandReturnType
    {
        [CanExecuteCommand("INCORRECT.METHOD")]
        public void IncorrectMethod() { }
    }
}
