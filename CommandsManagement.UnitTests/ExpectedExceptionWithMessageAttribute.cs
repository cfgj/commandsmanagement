﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace CommandsManagement.UnitTests
{
    public class ExpectedExceptionWithMessageAttribute : ExpectedExceptionBaseAttribute
    {
        public Type ExceptionType { get; set; }

        public string ExpectedMessage { get; set; }

        public ExpectedExceptionWithMessageAttribute(Type exceptionType)
        {
            ExceptionType = exceptionType;
        }

        public ExpectedExceptionWithMessageAttribute(Type exceptionType, string expectedMessage)
        {
            ExceptionType = exceptionType;
            ExpectedMessage = expectedMessage;
        }

        protected override void Verify(Exception ex)
        {
            if (ex.GetType() != ExceptionType)
            {
                string message = "{0} failed. Expected exception type: {1}. Actual exception type: {2}. Exception message: {3}";
                Assert.Fail(String.Format(message, GetType().Name, ExceptionType.FullName, ex.GetType().FullName, ex.Message));
            }

            var actualMessage = ex.Message.Trim();

            if (ExpectedMessage != null)
            {
                Assert.AreEqual(ExpectedMessage, actualMessage);
            }
        }
    }
}
