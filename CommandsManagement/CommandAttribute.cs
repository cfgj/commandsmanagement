﻿using System;

namespace CommandManagement
{
    /// <summary>
    /// PL: Opisuje command handler typu Execute, który może byc wywoływany podczas jakiegoś zdarzenia,
    /// np. kliknięcie (Click).
    /// EN: Describes a Execute command handler that can be invoked when an event occurs, e.g. Click event.
    /// </summary>
    /// <example>
    /// PL: Poniższy przykład pokazuje jak korzystać z atrybutu ExecuteCommand.
    /// EN: The following code example demonstrates how to use the ExecuteCommand attribute.
    /// <code>
    /// [ExecuteCommand("EDIT.COPY")]
    /// public void OnCopy(CommandArgs args)
    /// {
    ///     Clipboard.SetDataObject(Editor.SelectedText);
    /// }
    /// </code>
    /// </example>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
    public class ExecuteCommandAttribute : Attribute
    {
        /// <summary>
        /// Initilizes a new instance of the ExecuteCommandAttribute class with a command name.
        /// </summary>
        /// <param name="commandName">Command name.</param>
        public ExecuteCommandAttribute(string commandName) 
        {
            CommandName = commandName;
        }

        /// <summary>
        /// Command name.
        /// </summary>
        public string CommandName { get; set; }

    }

    /// <summary>
    /// PL: Opisuje command handler typu CanExecute, który sprawdza, czy jakaś inna komenda może być wywołana.
    /// Może również służyć do aktualizowania wartości jakichś pól, itp.
    /// EN: Describes a CanExecute command handler that can determine whether the command can be executed.
    /// It also can be use to changing the value of a field, etc.
    /// </summary>
    /// <example>
    /// The following code example demonstrates how to use the CanExecuteCommand attribute.
    /// <code>
    /// [CanExecuteCommand("EDIT.COPY")]
    /// public bool UpdateCopyCommand(CommandArgs args)
    /// {
    ///     return Editor.SelectedText.Length > 0;
    /// }
    /// </code>
    /// </example>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
    public class CanExecuteCommandAttribute : Attribute
    {
        /// <summary>
        /// Initilizes a new instance of the CanExecuteCommandAttribute class with a command name.
        /// </summary>
        /// <param name="commandName">Command name.</param>
        public CanExecuteCommandAttribute(string commandName) 
        {
            CommandName = commandName;
        }

        /// <summary>
        /// Command name.
        /// </summary>
        public string CommandName { get; set; }
    }
}
