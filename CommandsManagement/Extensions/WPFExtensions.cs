﻿using System.Collections;
using System.Collections.Generic;
using System.Windows;

namespace CommandManagement.WpfExtensions
{
    /// <summary>
    /// Klasa z rozszerzeniem do WPF.
    /// </summary>
    public static class WpfExtensions
    {
        /// <summary>
        /// Propercja rozszerzająca klasy dziedziczące po DependecyObject o pole
        /// przechowywujące nazwę komendy.
        /// </summary>
        public static readonly DependencyProperty CommandName =
            DependencyProperty.RegisterAttached("CommandName", typeof(string), typeof(WpfExtensions), new PropertyMetadata(default(string)));

        /// <summary>
        /// Ustawia wartość propercji CommandName.
        /// </summary>
        /// <param name="element">Instancja klasy UIElement, np. kontrolka Button.</param>
        /// <param name="value">Nazwa komendy.</param>
        public static void SetCommandName(UIElement element, string value)
        {
            element.SetValue(CommandName, value);
        }

        /// <summary>
        /// Pobiera wartość propercji CommandName.
        /// </summary>
        /// <param name="element">Instancja klasy UIElement, np. kontrolka Botton.</param>
        /// <returns></returns>
        public static string GetCommandName(UIElement element)
        {
            return (string) element.GetValue(CommandName);
        }

        /// <summary>
        /// Zwraca listę elementów z niepustą nazwą komendy.
        /// </summary>
        /// <typeparam name="T">Typ elementów, które chcemy dostać w wyniku.</typeparam>
        /// <param name="element">Korzeń drzewa elementów.</param>
        /// <returns></returns>
        public static IEnumerable<T> GetUIElementsWithCommandOftype<T>(this UIElement element) where T : UIElement
        {
            IEnumerable children = LogicalTreeHelper.GetChildren(element);

            foreach (object child in children)
            {
                var uiElement = child as UIElement;

                if (uiElement != null)
                {
                    if (child is T && uiElement.GetValue(WpfExtensions.CommandName) != null)
                    {
                        yield return child as T;
                    }

                    foreach (var grandchild in GetUIElementsWithCommandOftype<T>(uiElement))
                    {
                        if (grandchild.GetValue(WpfExtensions.CommandName) != null)
                        {
                            yield return grandchild;
                        }
                    }
                }
            }
        }
    }
}
