using System;
using System.Collections.Generic;

namespace CommandManagement
{
    /// <summary>
    /// Handles all communication between CommandManager and Command instance
    /// and executes commands.
    /// </summary>
	public abstract class CommandExecutor
	{
        private IDictionary<object, Command> commands;

        /// <summary>
        /// Initializes a new instance of the CommandExecutor.
        /// </summary>
        public CommandExecutor()
        {
            commands = new Dictionary<object, Command>();
        }

        /// <summary>
        /// Adds sender and command instance to CommandExecutor.
        /// You can override this method to subscribe to selected 
        /// event Execute ecent handler from this class.
        /// Example:
        /// <code>
        /// public override void AttachEvent(object instance)
        /// {
        ///     MenuItem menuItem = (MenuItem) instance;
        ///     menuItem.Click += Execute;
        /// }
        /// </code>
        /// </summary>
        /// <param name="instance"></param>
		public virtual void AttachEvents(object instance)
		{
			// some code...
		}

        /// <summary>
        /// Enables or disables sender based on CanExecuteCommand.
        /// By default this method does nothing.
        /// Example:
        /// <code>
        /// public override void Enable(object sender, bool enable)
        /// {
        ///     MenuItem menuItem = (MenuItem) item;
        ///     menuItem.Enabled = enable;
        /// }
        /// </code>
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="enable"></param>
        public virtual void Enable(object instance, bool enable)
        {
            // some code...
        }

        /// <summary>
        /// Invokes command based on sender.
        /// </summary>
        /// <param name="instance">Sender object.</param>
        /// <param name="e">Event data.</param>
        public virtual void Execute(object instance, EventArgs e)
        {
            Command command = GetCommand(instance);
            command.Execute(instance);
        }

        internal void AddInstance(object instance, Command command)
        {
            AttachEvents(instance);

            commands.Add(instance, command);
        }

		private Command GetCommand(object instance)
		{
			return commands[instance];
		}
	}
}
