﻿using System;

namespace CommandManagement
{
    /// <summary>
    /// 
    /// </summary>
    public interface ICommandsHandler
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="owner"></param>
        void RegisterAttributedCommandHandlers(object owner);
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="commandName"></param>
        void UnregisterCommandHandler(string commandName);

        /// <summary>
        /// 
        /// </summary>
        void ClearCache();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="commandName"></param>
        /// <returns></returns>
        bool ContainsCommandHandler(CommandHandlerType type, string commandName);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        void ExecuteCommandHandler(CommandArgs args);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="commandName"></param>
        /// <returns></returns>
        Action<CommandArgs> GetExecuteCommandHandler(string commandName);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        bool CanExecuteCommandHandler(CommandArgs args);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="commandName"></param>
        /// <returns></returns>
        Func<CommandArgs, bool> GetCanExecuteCommandHandler(string commandName);
    }
}
