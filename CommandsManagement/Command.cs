using System;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;

namespace CommandManagement
{
	internal class Command
    {
        private Action<CommandArgs> executeCommand;

        private Func<CommandArgs, bool> canExecuteCommand;

        private CommandExecutor commandExecutor;

        /// <summary>
        /// Initializes a new instance of the Command.
        /// </summary>
        /// <param name="name">Command name.</param>
        /// <param name="handler">Commands handler instance.</param>
        /// <param name="executor">Command executor instance.</param>
		public Command(string name, ICommandsHandler handler, CommandExecutor executor)
		{
            CommandName = name;
            commandExecutor = executor;
            CommandPublishers = new ObservableCollection<object>();

            CommandPublishers.CollectionChanged += new NotifyCollectionChangedEventHandler(ObservableCollectionChanged);

            SetCommands();
		}

        public string CommandName
        {
            get;
            private set;
        }

        public ObservableCollection<object> CommandPublishers
        {
            get;
            private set;
        }

        private ICommandsHandler CommandHandler
        {
            get;
            set;
        }

        /// <summary>
        /// Invokes a command.
        /// </summary>
        /// <param name="sender">Sender instance.</param>
        public void Execute(object sender)
		{
            if (executeCommand != null)
            {
                executeCommand(new CommandArgs { Sender = sender, CommandName = CommandName });
            }
		}

        /// <summary>
        /// Invokes a CanExecute command.
        /// </summary>
        public void RefreshCanExecute()
		{
            if (canExecuteCommand != null)
            {
                foreach (object instance in CommandPublishers)
                {
                    bool canExecute = canExecuteCommand(new CommandArgs { Sender = instance, CommandName = CommandName });

                    commandExecutor.Enable(instance, canExecute);
                }
            }
		}

        /// <summary>
        /// Returns a string that represents the command object.
        /// </summary>
        /// <returns>Command name.</returns>
        public override string ToString()
        {
            return CommandName;
        }

        private void SetCommands()
        {
            if (CommandHandler.ContainsCommandHandler(CommandHandlerType.Execute, CommandName))
            {
                executeCommand = CommandHandler.GetExecuteCommandHandler(CommandName);
            }

            if (CommandHandler.ContainsCommandHandler(CommandHandlerType.CanExecute, CommandName))
            {
                canExecuteCommand = CommandHandler.GetCanExecuteCommandHandler(CommandName);
            }
        }

        private void ObservableCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (var item in e.NewItems)
                {
                    commandExecutor.AddInstance(item, this);
                }
            }
        }
	}
}
