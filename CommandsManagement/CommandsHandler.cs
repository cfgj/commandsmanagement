﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Reflection;

namespace CommandManagement
{
    /// <summary>
    /// Represents a commands handler thet can invoke command handlers by its name.
    /// </summary>
    public class CommandsHandler : ICommandsHandler
    {
        private static IDictionary<Type, IList<CommandInfo>> handlersCache = new Dictionary<Type, IList<CommandInfo>>();

        private IDictionary<string, Action<CommandArgs>> executeHandlers = new Dictionary<string, Action<CommandArgs>>();

        private IDictionary<string, Func<CommandArgs, bool>> canExecuteHandlers = new Dictionary<string, Func<CommandArgs, bool>>();

        /// <summary>
        /// Registers command handlers defined in owner object marked as
        /// ExecuteCommandHandler or CanExecuteCommandHandler.
        /// </summary>
        /// <param name="owner">The object to search for command handlers.</param>
        public void RegisterAttributedCommandHandlers(object owner)
        {
            if (owner == null)
            {
                throw new ArgumentException("owner cannot be null.");
            }

            Type type = owner.GetType();
            IList<CommandInfo> listInfo;

            if ( ! handlersCache.TryGetValue(type, out listInfo))
            {
                listInfo = ScanAttributedHandlers(type);
                handlersCache[type] = listInfo;
            }

            RegisterAttributedHandlers(owner, listInfo);
        }

        /// <summary>
        /// Unregisters command handler.
        /// </summary>
        /// <param name="commandName">Command handler name.</param>
        public void UnregisterCommandHandler(string commandName)
        {
            executeHandlers.Remove(commandName);
            canExecuteHandlers.Remove(commandName);
        }

        /// <summary>
        /// Clears cache storing informations about methods in registered attributed command handlers.
        /// </summary>
        public void ClearCache()
        {
            handlersCache.Clear();
        }

        /// <summary>
        /// Verifies that the specified command handler is registered.
        /// </summary>
        /// <param name="type">Command handler type.</param>
        /// <param name="commandName">Command handler name.</param>
        /// <returns>true if the specified command handler is registerd; otherwise, false.</returns>
        public bool ContainsCommandHandler(CommandHandlerType type, string commandName)
        {
            if (type == CommandHandlerType.Execute)
            {
                return executeHandlers.ContainsKey(commandName);
            }
            else
            {
                return canExecuteHandlers.ContainsKey(commandName);
            }
        }

        /// <summary>
        /// Invokes Execute command handler.
        /// </summary>
        /// <param name="args">An object that contains event data.</param>
        public void ExecuteCommandHandler(CommandArgs args)
        {
            if ( ! executeHandlers.ContainsKey(args.CommandName))
                throw new ArgumentException(string.Format("Command handler {0} does not exist.", args.CommandName));

            executeHandlers[args.CommandName](args);
        }

        /// <summary>
        /// Gets Execute command handler.
        /// </summary>
        /// <param name="commandName">Command handler name.</param>
        /// <returns>Delegate that encapsulates Execute command handler.</returns>
        public Action<CommandArgs> GetExecuteCommandHandler(string commandName)
        {
            if ( ! executeHandlers.ContainsKey(commandName))
                throw new ArgumentException(string.Format("Command handler {0} does not exist.", commandName));

            return executeHandlers[commandName];
        }

        /// <summary>
        /// Invokes CanExecute command handler.
        /// </summary>
        /// <param name="args">An object that contains event data.</param>
        /// <returns>The value returned by the command handler.</returns>
        public bool CanExecuteCommandHandler(CommandArgs args)
        {
            if ( ! canExecuteHandlers.ContainsKey(args.CommandName))
                throw new ArgumentException(string.Format("Command handler {0} does not exist.", args.CommandName));

            return canExecuteHandlers[args.CommandName](args);
        }

        /// <summary>
        /// Gets CanExecute command handler.
        /// </summary>
        /// <param name="commandName">Command handler name.</param>
        /// <returns>Delegate that encapsulates CanExecute command handler.</returns>
        public Func<CommandArgs, bool> GetCanExecuteCommandHandler(string commandName)
        {
            if ( ! canExecuteHandlers.ContainsKey(commandName))
                throw new ArgumentException(string.Format("Command handler {0} does not exist.", commandName));

            return canExecuteHandlers[commandName];
        }

        private void RegisterAttributedHandlers(object owner, IList<CommandInfo> listInfo)
        {
            foreach (CommandInfo commandInfo in listInfo)
            {
                if (commandInfo.IsCanExecute)
                {
                    RegisterCanExecuteHandler(commandInfo.CommandName, CreateCanExecuteDelegate(owner, commandInfo.MethodInfo));
                }
                else
                {
                    RegisterExecuteHandler(commandInfo.CommandName, CreateExecuteDelegate(owner, commandInfo.MethodInfo));
                }
            }
        }

        private IList<CommandInfo> ScanAttributedHandlers(Type type)
        {
            IList<CommandInfo> listInfo = new List<CommandInfo>();

            foreach (MethodInfo methodInfo in type.GetMethods())
            {
                foreach (Attribute attribute in methodInfo.GetCustomAttributes(false))
                {
                    if (attribute.GetType() == typeof(ExecuteCommandAttribute))
                    {
                        listInfo.Add(new CommandInfo
                        {
                            CommandName = ((ExecuteCommandAttribute) attribute).CommandName,
                            MethodInfo = methodInfo,
                            IsCanExecute = false
                        });
                    }
                    else if (attribute.GetType() == typeof(CanExecuteCommandAttribute))
                    {
                        listInfo.Add(new CommandInfo
                        {
                            CommandName = ((CanExecuteCommandAttribute) attribute).CommandName,
                            MethodInfo = methodInfo,
                            IsCanExecute = true
                        });
                    }
                }
            }

            return listInfo;
        }

        private Action<CommandArgs> CreateExecuteDelegate(object owner, MethodInfo methodInfo)
        {
            CheckParameters(methodInfo);
            CheckReturnType(CommandHandlerType.Execute, methodInfo);

            if (methodInfo.GetParameters().Length == 1)
            {
                return (Action<CommandArgs>) Delegate.CreateDelegate(typeof(Action<CommandArgs>), owner, methodInfo);
            }

            Action action = (Action) Delegate.CreateDelegate(typeof(Action), owner, methodInfo);

            return commandArgs => action();
        }

        private Func<CommandArgs, bool> CreateCanExecuteDelegate(object owner, MethodInfo methodInfo)
        {
            CheckParameters(methodInfo);
            CheckReturnType(CommandHandlerType.CanExecute, methodInfo);

            if (methodInfo.GetParameters().Length == 1)
            {
                return (Func<CommandArgs, bool>) Delegate.CreateDelegate(typeof(Func<CommandArgs, bool>), owner, methodInfo);
            }

            Func<bool> func = (Func<bool>) Delegate.CreateDelegate(typeof(Func<bool>), owner, methodInfo);

            return commandArgs => func();
        }

        private void CheckParameters(MethodInfo methodInfo)
        {
            ParameterInfo[] parameters = methodInfo.GetParameters();

            if (parameters.Length > 1 || (parameters.Length == 1 && parameters[0].ParameterType != typeof(CommandArgs)))
            {
                throw new ArgumentException(string.Format("{0} method must has one CommandArgs type parameter or none.", methodInfo.Name));
            }
        }

        private void CheckReturnType(CommandHandlerType commandHandlerType, MethodInfo methodInfo)
        {
            if (commandHandlerType == CommandHandlerType.Execute && methodInfo.ReturnType != typeof(void))
            {
                throw new ArgumentException(string.Format("{0} method cannot return any value.", methodInfo.Name));
            } 
            else if (commandHandlerType == CommandHandlerType.CanExecute && methodInfo.ReturnType != typeof(bool))
            {
                throw new ArgumentException(string.Format("{0} method must return a result of type bool.", methodInfo.Name));
            }
        }

        private void RegisterExecuteHandler(string commandName, Action<CommandArgs> executeHandler)
        {
            executeHandlers[commandName] = executeHandler;
        }

        private void RegisterCanExecuteHandler(string commandName, Func<CommandArgs, bool> canExecuteHandler)
        {
            canExecuteHandlers[commandName] = canExecuteHandler;
        }

        private class CommandInfo
        {
            public string CommandName { get; set; }

            public MethodInfo MethodInfo { get; set; }

            public bool IsCanExecute { get; set; }
        }
    }
}
