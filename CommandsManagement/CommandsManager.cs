﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace CommandManagement
{
    /// <summary>
    /// Manages command executors and commands handlers.
    /// </summary>
    public class CommandsManager
    {
        private IDictionary<string, Command> commandsList;

        private IDictionary<Type, CommandExecutor> commandExecutors;

        private ICommandsHandler commandsHandler;

        /// <summary>
        /// Initializes a new instance of the CommandManager with default commands hander.
        /// </summary>
        public CommandsManager()
        {
            commandsList = new Dictionary<string, Command>();
            commandExecutors = new Dictionary<Type, CommandExecutor>();
            commandsHandler = new CommandsHandler();
        }

        /// <summary>
        /// Gets commands handler instance.
        /// </summary>
        public ICommandsHandler CommandsHandler
        {
            get
            {
                return commandsHandler;
            }
        }

        /// <summary>
        /// Initializes a new instance of the CommandManager with the specified commands handler.
        /// </summary>
        /// <param name="handler">The commands hander obejct.</param>
        public CommandsManager(ICommandsHandler handler) : base()
        {
            if (handler == null)
            {
                throw new ArgumentException("handler cannot be null.");
            }

            commandsHandler = handler;
        }

        /// <summary>
        /// Registers command executor.
        /// Many command executors can be registerd.
        /// </summary>
        /// <param name="type">Type of element.</param>
        /// <param name="executor">Executor for senders of given type.</param>
        public void RegisterCommandExecutor(Type type, CommandExecutor executor)
        {
            commandExecutors.Add(type, executor);
        }

        /// <summary>
        /// Registers senders, eg. Button or MenuItem.
        /// Example: 
        /// <code>
        /// RegisterSenders{MenuItem}(senders, s => s.Name)
        /// </code>
        /// </summary>
        /// <param name="senders">Senders collection.</param>
        /// <param name="getCommandNameAssociatedWithSender">Delagate returns command name.</param>
        public void RegisterSenders<T>(IEnumerable<T> senders, Func<T, string> getCommandNameAssociatedWithSender)
        {
            foreach (var sender in senders)
            {
                var name = getCommandNameAssociatedWithSender(sender);

                if ( ! commandsList.ContainsKey(name))
                {
                    Command command = new Command(name, commandsHandler, GetCommandExecutor(sender));

                    commandsList.Add(command.CommandName, command);
                }

                commandsList[name].CommandPublishers.Add(sender);
            }
        }

        /// <summary>
        /// The method should be invoked when application enter the idle state.
        /// Example: <code>Application.Idle = new EventHandler(CommandManager.OnIdle)</code>
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="args">Event arguments.</param>
        public void OnIdle(object sender, EventArgs args)
        {
            foreach (Command command in commandsList.Values)
            {
                command.RefreshCanExecute();
            }
        }

        /// <summary>
        /// Zwraca instancję klasy CommandExecutor na podstawie obiektu nadawcy.
        /// </summary>
        /// <param name="instance">Obiekt nadawcy, np. MenuItem.</param>
        /// <returns>Instancja klasy CommandExecutor.</returns>
        private CommandExecutor GetCommandExecutor(object instance)
        {
            Type type = instance.GetType();

            if ( ! commandExecutors.ContainsKey(type))
            {
                throw new Exception(string.Format("For type {0} does not exist any command executor.", type.Name));
            }

            return commandExecutors[type];
        }
    }
}
