﻿using System;

namespace CommandManagement
{
    /// <summary>
    /// PL: Opisuje rodzaj command handlera.
    /// EN: Specifies the type for a command handler.
    /// </summary>
    /// <remarks>
    /// PL: Wykorzystaj wartości tego enumeratora do określenia rodzaju command handlera
    /// korzystając z funkcji CommandsHandler.ContainsCommandHandler.
    /// EN: Use the members of this enumerations to determine the command handler type 
    /// using <see cref="CommandsHandler.ContainsCommandHandler" />.
    /// </remarks>
    /// <example>
    /// PL: Poniższy przykład pokazuje jak wykorzystać enumerator CommandHandlerType i funkcję
    /// CommandsHandler.ContainsCommandHandler.
    /// EN: The following code example demonstrates how to use the CommandHandlerType enumeration
    /// and the <see cref="CommandsHandler.ContainsCommandHandler" />.
    /// <code>
    /// CommandsHandler commandsHandler = new CommandsHandler();
    /// // some code...
    /// commandsHandler.ContainsCommandHandler(CommandHandlerType.Execute, "CRUD.DELETE");
    /// </code>
    /// </example>
    public enum CommandHandlerType : byte
    {
        /// <summary>
        /// PL: Opisuje command handlera wywoływanego podczas jakiegoś zdarzenia, np. kliknięcie (Click)
        /// EN: A command handler invoked when an event occurs, e.g. Click event.
        /// </summary>
        Execute = 0,

        /// <summary>
        /// PL: Opisuje command handlera, który sprawdza, czy jakaś inna komenda może być wywołana.
        /// Może również służyć do aktualizowania wartości jakichś pól, itp.
        /// EN: A command handler that can determine whether the command can be executed.
        /// Can be use to changing the value of a field, etc.
        /// </summary>
        CanExecute = 1
    }
}
