﻿using System;

namespace CommandManagement
{
    /// <summary>
    /// Represents command handler arguments.
    /// </summary>
    public class CommandArgs
    {
        /// <summary>
        /// Gets the command name.
        /// </summary>
        public string CommandName 
        { 
            get; 
            internal set; 
        }

        /// <summary>
        /// Gets sender object, eg. Button, MenuItem.
        /// </summary>
        public object Sender 
        { 
            get; 
            internal set; 
        }
    }
}
