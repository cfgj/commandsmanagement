﻿using System;
using System.Windows.Controls;
using CommandManagement;
using System.Windows.Controls.Primitives;

namespace WpfExample
{
    class MenuCommandExecutor : CommandExecutor
    {
        public override void AttachEvents(object instance)
        {
            MenuItem menuItem = (MenuItem) instance;
            menuItem.Click += Execute;
        }

        public override void Enable(object instance, bool enable)
        {
            MenuItem menuItem = (MenuItem) instance;
            menuItem.IsEnabled = enable;
        }

        public override void Execute(object instance, EventArgs e)
        {
            base.Execute(instance, e);
        }
    }

    class StatusBarCommandExecutor : CommandExecutor
    {
        public override void Enable(object instance, bool enable)
        {
            StatusBarItem menuItem = (StatusBarItem) instance;
            menuItem.IsEnabled = enable;

            base.Enable(instance, enable);
        }
    }
}
