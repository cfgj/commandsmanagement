﻿using CommandManagement;
using CommandManagement.WpfExtensions;
using System;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;
using System.Windows.Input;

namespace WpfExample
{
    public partial class MainWindow : Window
    {
        private CommandsManager CommandManager;

        public MainWindow()
        {
            InitializeComponent();
            InitializeCommandManager();
        }

        private void InitializeCommandManager()
        {
            CommandManager = new CommandsManager();

            CommandManager.CommandsHandler.RegisterAttributedCommandHandlers(this);

            CommandManager.RegisterCommandExecutor(typeof(MenuItem), new MenuCommandExecutor());
            CommandManager.RegisterCommandExecutor(typeof(StatusBarItem), new StatusBarCommandExecutor());
            CommandManager.RegisterSenders(this.GetUIElementsWithCommandOftype<UIElement>(), 
                                           s => (string) s.GetValue(WpfExtensions.CommandName));

            Dispatcher.Hooks.DispatcherInactive += new EventHandler(CommandManager.OnIdle);
        }

        #region Command handlers

        [ExecuteCommand("FILE.OPEN")]
        [ExecuteCommand("FILE.SAVE")]
        public void CommandHandler(CommandArgs args)
        {
            var menuItem = args.Sender as MenuItem;

            StringBuilder sb = new StringBuilder();
            sb.Append("Command name: ");
            sb.AppendLine(args.CommandName);

            if (menuItem != null)
            {
                sb.Append("Sender caption: ");
                sb.Append(menuItem.Header);
            }

            MessageBox.Show(sb.ToString(), "Command Executed");
        }

        [CanExecuteCommand("EDIT.COPY")]
        public bool UpdateCopyCommand(CommandArgs args)
        {
            return Editor.Selection.Text.Length > 0;
        }

        [ExecuteCommand("EDIT.COPY")]
        public void OnCopy(CommandArgs args)
        {
            Clipboard.SetDataObject(Editor.Selection.Text);
        }

        [CanExecuteCommand("EDIT.SELECTALL")]
        public bool UpdateSelectAllCommand(CommandArgs args)
        {
            var menuItem = args.Sender as MenuItem;

            string text = new TextRange(Editor.Document.ContentStart, Editor.Document.ContentEnd).Text;

            if (menuItem != null)
            {
                menuItem.IsChecked = text.Length > 0 && Editor.Selection.Text == text;
            }

            return text.Length > 0 && text != "\r\n";
        }

        [ExecuteCommand("EDIT.SELECTALL")]
        public void OnSelectAll(CommandArgs args)
        {
            string text = new TextRange(Editor.Document.ContentStart, Editor.Document.ContentEnd).Text;

            if (Editor.Selection.Text == text)
            {
                var start = Editor.Document.ContentStart;

                Editor.Selection.Select(start, start);
            }
            else
            {
                Editor.SelectAll();
            }
        }

        [CanExecuteCommand("STATUS.CURRENTLINE")]
        public bool UpdateCurrentLine(CommandArgs args)
        {
            int lineMoved;

            TextPointer tp = Editor.Selection.Start;
            tp.GetLineStartPosition(-int.MaxValue, out lineMoved);

            int line = -lineMoved + 1;

            CurrentLine.Content = string.Format("Line: {0}", line);

            return true;
        }

        [CanExecuteCommand("STATUS.CURRENTCOLUMN")]
        public bool UpdateCurrentColumn(CommandArgs args)
        {
            TextPointer tp = Editor.Selection.Start;

            int column = tp.GetLineStartPosition(0).GetOffsetToPosition(tp) + 1;

            CurrentColumn.Content = string.Format("Column: {0}", column);

            return true;
        }

        #endregion
    }
}
