using CommandManagement;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsExample
{
    public class MainForm : Form
    {
		private CommandsManager CommandManager;
        private MainMenu MainMenu;
		private RichTextBox Editor;
		private MenuItem FileMenuItem;
		private MenuItem EditFIleItem;
        private MenuItem SelectAll;
        private MenuItem FileOpen;
        private MenuItem FileSave;
        private MenuItem EditCopy;
        private StatusStrip StatusStrip;
        private ToolStripStatusLabel CurrentLine;
        private ToolStripStatusLabel CurrentColumn;
        private IContainer components = null;

        [STAThread]
        static void Main()
        {
            Application.Run(new MainForm());
        }

        public MainForm()
        {
            InitializeComponent();
			InitializeCommandManager();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null) 
                {
                    components.Dispose();
                }
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Editor = new System.Windows.Forms.RichTextBox();
            this.FileOpen = new System.Windows.Forms.MenuItem();
            this.FileSave = new System.Windows.Forms.MenuItem();
            this.FileMenuItem = new System.Windows.Forms.MenuItem();
            this.EditCopy = new System.Windows.Forms.MenuItem();
            this.EditFIleItem = new System.Windows.Forms.MenuItem();
            this.SelectAll = new System.Windows.Forms.MenuItem();
            this.MainMenu = new System.Windows.Forms.MainMenu(this.components);
            this.StatusStrip = new System.Windows.Forms.StatusStrip();
            this.CurrentLine = new System.Windows.Forms.ToolStripStatusLabel();
            this.CurrentColumn = new System.Windows.Forms.ToolStripStatusLabel();
            this.StatusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // Editor
            // 
            this.Editor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Editor.Location = new System.Drawing.Point(0, 0);
            this.Editor.Name = "Editor";
            this.Editor.Size = new System.Drawing.Size(359, 164);
            this.Editor.TabIndex = 3;
            this.Editor.Text = "";
            // 
            // FileOpen
            // 
            this.FileOpen.Index = 0;
            this.FileOpen.Tag = "FILE.OPEN";
            this.FileOpen.Text = "Open";
            // 
            // FileSave
            // 
            this.FileSave.Index = 1;
            this.FileSave.Tag = "FILE.SAVE";
            this.FileSave.Text = "Save";
            // 
            // FileMenuItem
            // 
            this.FileMenuItem.Index = 0;
            this.FileMenuItem.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.FileOpen,
            this.FileSave});
            this.FileMenuItem.Text = "File";
            // 
            // EditCopy
            // 
            this.EditCopy.Index = 1;
            this.EditCopy.Tag = "EDIT.COPY";
            this.EditCopy.Text = "Copy";
            // 
            // EditFIleItem
            // 
            this.EditFIleItem.Index = 1;
            this.EditFIleItem.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.SelectAll,
            this.EditCopy});
            this.EditFIleItem.Text = "Edit";
            // 
            // SelectAll
            // 
            this.SelectAll.Index = 0;
            this.SelectAll.Tag = "EDIT.SELECTALL";
            this.SelectAll.Text = "Select all";
            // 
            // MainMenu
            // 
            this.MainMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.FileMenuItem,
            this.EditFIleItem});
            // 
            // StatusStrip
            // 
            this.StatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CurrentLine,
            this.CurrentColumn});
            this.StatusStrip.Location = new System.Drawing.Point(0, 164);
            this.StatusStrip.Name = "StatusStrip";
            this.StatusStrip.Size = new System.Drawing.Size(359, 22);
            this.StatusStrip.TabIndex = 4;
            this.StatusStrip.Text = "StatusStrip";
            // 
            // CurrentLine
            // 
            this.CurrentLine.Name = "CurrentLine";
            this.CurrentLine.Size = new System.Drawing.Size(0, 17);
            this.CurrentLine.Tag = "STATUS.CURRENTLINE";
            // 
            // CurrentColumn
            // 
            this.CurrentColumn.Name = "CurrentColumn";
            this.CurrentColumn.Size = new System.Drawing.Size(0, 17);
            this.CurrentColumn.Tag = "STATUS.CURRENTCOLUMN";
            // 
            // MainForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(359, 186);
            this.Controls.Add(this.Editor);
            this.Controls.Add(this.StatusStrip);
            this.Menu = this.MainMenu;
            this.Name = "MainForm";
            this.Text = "Commands Manager Example";
            this.StatusStrip.ResumeLayout(false);
            this.StatusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

		private void InitializeCommandManager()
		{
            var menuSenders = new [] { FileOpen, FileSave, EditCopy, SelectAll };
            var statusStripSenders = new[] { CurrentLine, CurrentColumn };

            CommandManager = new CommandsManager();

            CommandManager.CommandsHandler.RegisterAttributedCommandHandlers(this);

            CommandManager.RegisterCommandExecutor(typeof(MenuItem), new MenuCommandExecutor());
            CommandManager.RegisterSenders(menuSenders, s => (string) s.Tag);

            CommandManager.RegisterCommandExecutor(typeof(ToolStripStatusLabel), new StatusBarCommandExecutor());
            CommandManager.RegisterSenders(statusStripSenders, s => (string) s.Tag);

            Application.Idle += new EventHandler(CommandManager.OnIdle);
		}

        #region Command handlers

        [ExecuteCommand("FILE.OPEN")]
        [ExecuteCommand("FILE.SAVE")]
        public void CommandHandler(CommandArgs args)
        {
            var menuItem = args.Sender as MenuItem;

            StringBuilder sb = new StringBuilder();
            sb.Append("Command name: ");
            sb.AppendLine(args.CommandName);

            if (menuItem != null)
            {
                sb.Append("Sender caption: ");
                sb.Append(menuItem.Text);
            }

            MessageBox.Show(sb.ToString(), "Command Executed");
        }

        [CanExecuteCommand("EDIT.COPY")]
        public bool UpdateCopyCommand(CommandArgs args)
        {
            return Editor.SelectedText.Length > 0;
        }

        [ExecuteCommand("EDIT.COPY")]
        public void OnCopy(CommandArgs args)
        {
            Clipboard.SetDataObject(Editor.SelectedText);
        }

        [CanExecuteCommand("EDIT.SELECTALL")]
        public bool UpdateSelectAllCommand(CommandArgs args)
		{
            var menuItem = args.Sender as MenuItem;

            if (menuItem != null)
            {
                menuItem.Checked = Editor.SelectedText.Length > 0 && Editor.SelectedText == Editor.Text;
            }

			return Editor.Text.Length > 0;
		}

        [ExecuteCommand("EDIT.SELECTALL")]
        public void OnSelectAll(CommandArgs args)
        {
            if (Editor.SelectedText == Editor.Text)
            {
                Editor.Select(0, 0);
            }
            else
            {
                Editor.SelectAll();
            }
        }

        [CanExecuteCommand("STATUS.CURRENTLINE")]
        public bool UpdateCurrentLine(CommandArgs args)
        {
            int line = Editor.GetLineFromCharIndex(Editor.SelectionStart) + 1;

            var currentLineLabel = args.Sender as ToolStripLabel;

            if (currentLineLabel != null)
            {
                currentLineLabel.Text = string.Format("Line: {0}", line);
            }

            return true;
        }

        [CanExecuteCommand("STATUS.CURRENTCOLUMN")]
        public bool UpdateCurrentColumn(CommandArgs args)
        {
            int startingPoint = Editor.SelectionStart;
            int column = startingPoint - Editor.GetFirstCharIndexFromLine(Editor.GetLineFromCharIndex(startingPoint)) + 1;

            var currentColumnLabel = args.Sender as ToolStripLabel;

            if (currentColumnLabel != null)
            {
                currentColumnLabel.Text = string.Format("Column: {0}", column);
            }

            return true;
        }

        #endregion
    }
}
