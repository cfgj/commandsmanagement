﻿using CommandManagement;
using System;
using System.Windows.Forms;

namespace WindowsFormsExample
{
    class MenuCommandExecutor : CommandExecutor
    {
        public override void AttachEvents(object instance)
        {
            MenuItem menuItem = (MenuItem) instance;
            menuItem.Click += Execute;
        }

        public override void Enable(object instance, bool enable)
        {
            MenuItem menuItem = (MenuItem) instance;
            menuItem.Enabled = enable;
        }

        public override void Execute(object instance, EventArgs e)
        {
            base.Execute(instance, e);
        }
    }

    class StatusBarCommandExecutor : CommandExecutor
    {
        public override void Enable(object instance, bool enable)
        {
            ToolStripStatusLabel menuItem = (ToolStripStatusLabel) instance;
            menuItem.Enabled = enable;

            base.Enable(instance, enable);
        }
    }
}
